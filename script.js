"use strict"; 

// 1. Що таке цикл в програмуванні?


// Цикл в програмуванні - це конструкція, яка дозволяє виконувати один чи декілька блоків коду декілька разів,
// Цикли дозволяють автоматизувати повторювані завдання і зменшити кількість коду.

// 2. Які види циклів є в JavaScript і які їх ключові слова?

//  1 Цикл for має три необов'язкових вирази.
// Ініціалізатор - Оператор for виконує ініціалізатор лише раз при початку циклу.
// Умова - це логічний вираз, який визначає, чи слід виконувати наступну ітерацію циклу for.
// Ітератор - Оператор for виконує ітератор після кожної ітерації.
// for (let i = 1; i < 5; i++) {
//     console.log(i); 
//    }

//  2 Цикл while

//  JavaScript має оператор `while`, який створює цикл, що виконує блок коду, доки вираз обчислюється як `true`
// let number = 5;
// let counter = 1;
// while (counter <= number) {
//   console.log(counter);
//   counter++;
// }

// 3. Чим відрізняється цикл do while від while?

// Оператор циклу `do...while` утворює цикл, що виконує блок коду до тих пір, поки умова оцінюється як `false`. 
// На відміну від циклу `while`, цикл `do...while` завжди виконує оператор принаймні один раз, перед оцінкою виразу

// 1. Запитайте у користувача два числа.
// Перевірте, чи є кожне з введених значень числом.
// Якщо ні, то запитуйте у користувача нове занчення до тих пір, поки воно не буде числом.
// Виведіть на екран всі цілі числа від меншого до більшого за допомогою циклу for.


let number1 = prompt("Enter the first number:");
while(isNaN(number1)){
  if (isNaN(number1)) {
  number1 = prompt("Enter the correct first number:");
}
}

let number2 = prompt("Enter the second number:");
while(isNaN(number2)){
  number1 = prompt("Enter the correct second number:");
}

let start = Math.min(number1, number2);
let end = Math.max(number1, number2);

for (let i = start; i <= end; i++) {
  console.log(i);
 }

// 2. Напишіть програму, яка запитує в користувача число та перевіряє, чи воно є парним числом. 
// Якщо введене значення не є парним числом, то запитуйте число доки користувач не введе правильне значення.

let number = prompt("Enter the number:");

while(isNaN(number) || number % 2 !== 0  ){
  alert("Wrong number. Please enter an even number");
  number=prompt("Please enter an even number:");
}

console.log("Number is entered:" + number) ;

 

